#!/usr/bin/env python3
'''
    File name: gp_santafe.py
    Author: Michal Charvát
    xcharv16@stud.fit.vutbr.cz
    FIT Brno University of Technology
    Date created: 25 Apr 2019
    References:
    	Artificial Ant Problem - DEAP documentation - https://deap.readthedocs.io/en/master/examples/gp_ant.html
    	Artificial Ant Problem - DEAP example - https://github.com/DEAP/deap/blob/82f774d9be6bad4b9d88272ba70ed6f1fca39fcf/examples/gp/ant.py
    	John R. Koza, “Genetic Programming I: On the Programming of Computers by Means of Natural Selection”, MIT Press, 1992, pages 147-161.
'''

from santafe_simulator import SantaFeSimulator, SantaFeTrailLoader, SantaFeTrail, AntOrientation
from functools import partial
from datetime import datetime
from time import sleep

import tkinter as tk
import numpy as np
import random
import matplotlib.pyplot as plt
import networkx as nx

from deap import algorithms
from deap import base
from deap import creator
from deap import tools
from deap import gp

class SantaFeGP:
	# GP function set, helpers
	# ------------------------------------------------------------
	@staticmethod
	def progn(*args):
		for arg in args:
			arg()

	@staticmethod
	def prog2(out1, out2):
		return partial(SantaFeGP.progn, out1, out2)

	@staticmethod
	def prog3(out1, out2, out3):
		return partial(SantaFeGP.progn, out1, out2, out3)

	def evaluate_individual(self, individual):
		# Transform the tree expression to functionnal Python code
		routine = gp.compile(individual, self.pset)
		self.simulator.execute_routine(routine)
		return self.simulator.food_eaten,

	def animate_individual(self, individual, animate_step_delay, repeat_condition=lambda:True):
		routine = gp.compile(individual, self.pset)
		while(repeat_condition()):
			self.simulator.execute_routine(routine, animate_step_delay)
			sleep(4)
	# ------------------------------------------------------------

	# plotting functions
	# ------------------------------------------------------------
	def init_plots(self):
		plt.ion()
		self.figure_plot = plt.figure(figsize=(15,8))
		self.tree_plot = self.figure_plot.add_subplot(211)
		self.fitness_plot = self.figure_plot.add_subplot(234)
		self.size_plot = self.figure_plot.add_subplot(235)
		self.gp_param_plot = self.figure_plot.add_subplot(236)

		self.gp_param_plot.set_axis_off()
		self.gp_param_plot.set_title('GP parameters')
		self.gp_param_plot.text(0, 0.5,
		                        'Ant max steps: {}\nPopulation size: {}\nGenerations: {}\n'
		                        'Crossover probability: {}\nMutation probability: {}\nTournament size: {}\nHall of fame size: {}\n'.format(
			                        self.ANT_MAX_STEPS, self.POPULATION_SIZE, self.GEN_SIZE, self.CROSS_PROB, self.MUT_PROB, self.TOURNAMENT_SIZE, self.HALL_OF_FAME_SIZE),
		                        fontsize=12, verticalalignment='center')

	def plot_fitness(self, logbook: tools.Logbook):
		self.fitness_plot.clear()

		keys = ['max', 'min', 'avg']
		for key in keys:
			plot_data = logbook.chapters['fitness'].select(key)
			self.fitness_plot.plot(plot_data, label=key)

		self.fitness_plot.set_xlabel('Generation')
		self.fitness_plot.set_ylabel('Raw fitness')
		self.fitness_plot.legend()
		plt.pause(0.0001)

	def plot_size(self, logbook: tools.Logbook):
		self.size_plot.clear()

		keys = ['max', 'min', 'avg']
		for key in keys:
			plot_data = logbook.chapters['size'].select(key)
			self.size_plot.plot(plot_data, label=key)

		self.size_plot.set_xlabel('Generation')
		self.size_plot.set_ylabel('Size')
		self.size_plot.legend()
		plt.pause(0.0001)

	def plot_individual(self, individual):
		self.tree_plot.clear()
		self.tree_plot.set_axis_off()
		self.tree_plot.set_title('Best individual')

		nodes, edges, labels = gp.graph(individual)
		g = nx.Graph()
		g.add_nodes_from(nodes)
		g.add_edges_from(edges)
		pos = nx.drawing.nx_agraph.graphviz_layout(g, prog="dot")

		nx.draw_networkx_nodes(g, pos, ax=self.tree_plot)
		nx.draw_networkx_edges(g, pos, ax=self.tree_plot)
		nx.draw_networkx_labels(g, pos, labels, ax=self.tree_plot)
		plt.pause(0.0001)
	# ------------------------------------------------------------
	def __del__(self):
		try:
			del FitnessMax
			del Individual
		except NameError:
			pass

	def __init__(self, **kwargs):
		# defaults
		self.CANVAS_WIDTH, self.CANVAS_HEIGHT = 1024, 786  # px
		self.SANTAFE_TRAIL_IN_FILE = 'koza_trail.pkl'
		self.ANT_MAX_STEPS = 600
		self.POPULATION_SIZE = 300
		self.GEN_SIZE = 40
		self.CROSS_PROB = 0.5
		self.MUT_PROB = 0.2
		self.HALL_OF_FAME_SIZE = 1
		self.TOURNAMENT_SIZE = 7
		self.DRAW_TRAIL = True

		# set GP parameters, overwrite defaults
		self.__dict__.update((k, v) for k, v in kwargs.items() if k in self.__dict__)

		# init plotting
		self.fig_plot = None
		self.fitness_plot = None
		self.size_plot = None
		self.tree_plot = None
		self.canvas = None

		# init TK
		if self.DRAW_TRAIL:
			self.master_tk = tk.Tk(className='SantaFe')
			self.canvas = tk.Canvas(self.master_tk, width=self.CANVAS_WIDTH, height=self.CANVAS_HEIGHT)
			self.canvas.pack()
			self.canvas.update()

	# init santafe simulator
		self.simulator = SantaFeSimulator(SantaFeTrailLoader(self.SANTAFE_TRAIL_IN_FILE), self.ANT_MAX_STEPS, self.canvas)
		self.simulator.draw()

		# function, terminal set
		self.pset = gp.PrimitiveSet("MAIN", 0)
		self.pset.addPrimitive(self.simulator.if_food_ahead, 2)
		self.pset.addPrimitive(self.prog2, 2)
		self.pset.addPrimitive(self.prog3, 3)
		self.pset.addTerminal(self.simulator.move_ahead)
		self.pset.addTerminal(self.simulator.turn_left)
		self.pset.addTerminal(self.simulator.turn_right)

		# setup the GP problem
		creator.create("FitnessMax", base.Fitness, weights=(1.0,))
		creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMax)

		self.toolbox = base.Toolbox()
		self.toolbox.register("expr_init", gp.genFull, pset=self.pset, min_=1, max_=2) # Attribute generator
		self.toolbox.register("individual", tools.initIterate, creator.Individual, self.toolbox.expr_init) # Structure initializers
		self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)

		self.toolbox.register("evaluate", self.evaluate_individual)
		self.toolbox.register("select", tools.selTournament, tournsize=self.TOURNAMENT_SIZE)
		self.toolbox.register('best', tools.selBest)
		self.toolbox.register("mate", gp.cxOnePoint)
		self.toolbox.register("expr_mut", gp.genFull, min_=0, max_=2)
		self.toolbox.register("mutate", gp.mutUniform, expr=self.toolbox.expr_mut, pset=self.pset)

	# evolution
	# reference: deap.algorithms.eaSimple
	def evolve(self, verbose=True, should_plot=True, rand_seed=None):
		# set random seed
		rand_seed = datetime.now().timestamp() if rand_seed is None else rand_seed
		random.seed(rand_seed)
		print('Random seed: {}\n'.format(rand_seed))

		# create logs
		stats_fit = tools.Statistics(lambda ind: ind.fitness.values)
		stats_size = tools.Statistics(lambda ind: float(len(ind)))
		mstats = tools.MultiStatistics(fitness=stats_fit, size=stats_size)
		mstats.register("avg", np.mean)
		mstats.register("min", np.min)
		mstats.register("max", np.max)
		# mstats.register("std", np.std)

		logbook = tools.Logbook()
		logbook.header = ['gen', 'nevals'] + (mstats.fields if mstats else [])

		# create initial population
		population = self.toolbox.population(n=self.POPULATION_SIZE)
		halloffame = tools.HallOfFame(self.HALL_OF_FAME_SIZE)


		# evaluate the individuals
		invalid_ind = [ind for ind in population if not ind.fitness.valid]
		fitnesses = self.toolbox.map(self.toolbox.evaluate, invalid_ind)
		for ind, fit in zip(invalid_ind, fitnesses):
			ind.fitness.values = fit

		if halloffame is not None:
			halloffame.update(population)

		best_individual = self.toolbox.best(population, 1)[0]

		# record generation 0
		record = mstats.compile(population) if mstats else {}
		logbook.record(gen=0, nevals=len(invalid_ind), **record)
		if verbose:
			print(logbook.stream)
		if should_plot:
			self.init_plots()

		# run evolution till max generation size or keyboard interrupt
		try:
			for generation in range(1, self.GEN_SIZE + 1):
				# Select the next generation individuals
				offspring = self.toolbox.select(population, len(population))

				# Vary the pool of individuals
				offspring = algorithms.varAnd(offspring, self.toolbox, self.CROSS_PROB, self.MUT_PROB)

				# Evaluate the individuals with an invalid fitness
				invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
				fitnesses = self.toolbox.map(self.toolbox.evaluate, invalid_ind)
				for ind, fit in zip(invalid_ind, fitnesses):
					ind.fitness.values = fit

				# Update the hall of fame with the generated individuals
				if halloffame is not None:
					halloffame.update(offspring)

				# Replace the current population by the offspring
				population[:] = offspring

				# get best one
				best_individual = self.toolbox.best(population, 1)[0]
				self.evaluate_individual(best_individual)
				self.simulator.draw()

				# Append the current generation statistics to the logbook
				record = mstats.compile(population) if mstats else {}
				logbook.record(gen=generation, nevals=len(invalid_ind), **record)
				if verbose:
					print(logbook.stream)
				if should_plot:
					self.plot_fitness(logbook)
					self.plot_size(logbook)
					self.plot_individual(best_individual)

		except KeyboardInterrupt:
			print('Interrupting evolution...\n') # stop evolution and return what we have

		print('Done.\n')
		return best_individual, population, logbook


# sample test run
if __name__ == "__main__":
	# available kwargs for SantaFeGP constructor
	# CANVAS_WIDTH
	# CANVAS_HEIGHT
	# SANTAFE_TRAIL_IN_FILE
	# ANT_MAX_STEPS
	# POPULATION_SIZE
	# GEN_SIZE
	# CROSS_PROB
	# MUT_PROB
	# HALL_OF_FAME_SIZE
	# TOURNAMENT_SIZE
	# DRAW_TRAIL

	santafe_gp = SantaFeGP(POPULATION_SIZE=20, GEN_SIZE=30, ANT_MAX_STEPS=600)
	# best_individual, population, logbook = santafe_gp.evolve(rand_seed=42)
	# best_individual, population, logbook = santafe_gp.evolve(rand_seed=69454)
	# best_individual, population, logbook = santafe_gp.evolve(rand_seed=496453453) # succeeds
	best_individual, population, logbook = santafe_gp.evolve()

	# show what we have found
	santafe_gp.plot_fitness(logbook)
	santafe_gp.plot_size(logbook)
	santafe_gp.plot_individual(best_individual)

	# animate best individual
	try:
		santafe_gp.animate_individual(best_individual, animate_step_delay=0.05)
	except KeyboardInterrupt:
		pass