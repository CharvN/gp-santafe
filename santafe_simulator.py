#!/usr/bin/env python3
'''
    File name: santafe_simulator.py
    Author: Michal Charvát
    xcharv16@stud.fit.vutbr.cz
    FIT Brno University of Technology
    Date created: 25 Apr 2019
'''

import PIL.Image, PIL.ImageTk
import tkinter as tk
import numpy as np
import random
import pickle
import math

from tkinter import messagebox
from functools import partial
from time import sleep
from enum import Enum

class AntOrientation(Enum):
	UP = 0
	RIGHT = 1
	DOWN = 2
	LEFT = 3


class Ant:
	_COORD_DIFFERENCES_FOR_ORIENTATION = ((-1,0), (0,1), (1,0), (0,-1))
	_ANGLE_FOR_ORIENTATION = (90, 0, 270, 180)

	def __init__(self, starting_position, orientation):
		self.y, self.x = starting_position
		self.orientation = orientation
		self.ant_image_original = None
		self.ant_image = None
		self.ant_image_tk = None

	def _get_coord_diff(self):
		return self._COORD_DIFFERENCES_FOR_ORIENTATION[self.orientation.value]

	def load_ant_image(self, cell_size):
		ant_image = PIL.Image.open('ant2.png').convert('RGBA')
		downscale_width = cell_size / ant_image.width
		downscale_height = cell_size / ant_image.height
		downscale = min(downscale_height, downscale_width)

		self.ant_image_original = ant_image.resize((math.ceil(ant_image.width * downscale), math.ceil(ant_image.height * downscale)))
		self.rotate_ant_image()

	def rotate_ant_image(self):
		self.ant_image = self.ant_image_original.rotate(self._ANGLE_FOR_ORIENTATION[self.orientation.value])

	def get_ahead_cell_coordinates(self):
		y_diff, x_diff = self._get_coord_diff()
		return self.y + y_diff, self.x + x_diff

	def move_ahead(self):
		self.y, self.x = self.get_ahead_cell_coordinates()

	def turn_left(self):
		self.orientation = AntOrientation((self.orientation.value-1) % 4)
		if self.ant_image_original is not None:
			self.rotate_ant_image()

	def turn_right(self):
		self.orientation = AntOrientation((self.orientation.value+1) % 4)

		if self.ant_image_original is not None:
			self.rotate_ant_image()


class SantaFeTrailLoader():
	def __init__(self, santafe_trail_in_file):
		self.santafe_param_in_file = santafe_trail_in_file


class SantaFeTrailBuilder:
	def __init__(self, dimensions):
		self.dimensions = dimensions


class SantaFeTrail:
	def __init__(self, dimensions, ant_starting_position, ant_orientation:AntOrientation, food):
		'''
		:param dimensions: tuple (y:int, x:int)
		:param ant_starting_position: tuple (y:int, x:int) starting position of an ant
		:param ant_orientation: AntOrientation
		:param food: set(tuple(y,x), ...) of coordinates OR int number of food to be randomly generated
		'''
		self.size_y, self.size_x = dimensions
		self.ant_starting_orientation = ant_orientation
		self.ant_starting_position = ant_starting_position
		self.food_coordinates = food if not isinstance(food, int) else self._generate_food_coordinates(food)

	def _generate_food_coordinates(self, food_count):
		random.seed()
		return { (random.randint(0, self.size_y-1), random.randint(0, self.size_x-1)) for _ in range(food_count)}

	def validate(self):
		assert (self.size_x > 0 and self.size_y > 0)
		assert (self.ant_starting_position[0] >= 0 and self.ant_starting_position[0] < self.size_y)
		assert (self.ant_starting_position[1] >= 0 and self.ant_starting_position[1] < self.size_x)
		assert (isinstance(self.ant_starting_orientation, AntOrientation))
		assert (isinstance(self.food_coordinates, set))
		for coord in self.food_coordinates:
			assert(isinstance(coord, tuple))
			y,x = coord
			assert(isinstance(x, int) and isinstance(y, int) and x >= 0 and x < self.size_x and y >= 0 and y < self.size_y)
		return True


class SantaFeSimulator:
	def __init__(self, santafe_trail, max_steps, canvas:tk.Canvas=None, santafe_trail_out_file=None):
		'''
		Initilizes santafe universe
		:param santafe_trail: SantaFeTrail, SantaFeTrailBuilder or SantaFeLoader
		:param max_steps: max steps an ant can take, then it stops
		:param canvas: tkinter canvas into which the model would draw
		:param santafe_trail_out_file: output filename to save current santafe trail into
		'''
		self.canvas = canvas
		self.max_steps = max_steps

		# defaults, loaded from trail struct and reset
		self.food_eaten = 0
		self.step_count = 0
		self.ant = None
		self.grid_visited = None
		self.grid_has_food = None
		self.animate_step_delay = None
		self.food_coordinates = set()
		self.ant_starting_position = (0, 0)
		self.ant_starting_orientation = AntOrientation.RIGHT

		# load saved trail
		if isinstance(santafe_trail, SantaFeTrailLoader):
			with open(santafe_trail.santafe_param_in_file, 'rb') as f:
				santafe_trail = pickle.load(f)

		# interactive mode - trail builder
		elif isinstance(santafe_trail, SantaFeTrailBuilder):
			self.build_trail(santafe_trail)
			santafe_trail = self.export_trail()

			# save santafe param file
			if santafe_trail_out_file:
				with open(santafe_trail_out_file, 'wb') as f:
					pickle.dump(santafe_trail, f)
				messagebox.showinfo("Custom SantaFe trail", 'Saved: {}'.format(santafe_trail_out_file))

		# load trail params
		assert(isinstance(santafe_trail, SantaFeTrail) and santafe_trail.validate())
		self.import_trail(santafe_trail)

		self.reset()

	def reset(self):
		# reset ant
		self.ant = Ant(self.ant_starting_position, self.ant_starting_orientation)
		if self.canvas:
			self.ant.load_ant_image(self._get_grid_edge_size())

		# reset internals
		self.food_eaten = 0
		self.step_count = 0
		self.animate_step_delay = None
		self.grid_visited = np.full((self.size_y, self.size_x), False, dtype='bool')
		self.grid_has_food = np.full((self.size_y, self.size_x), False, dtype='bool')

		# add food
		for food_y, food_x in self.food_coordinates:
			self.grid_has_food[food_y, food_x] = True

		# mark visited first cell
		self._ant_mark_visited()
	# ------------------------------------------------------------


	# Interactive trail builder
	# ------------------------------------------------------------
	def import_trail(self, santafe_trail:SantaFeTrail):
		self.size_y, self.size_x = santafe_trail.size_y, santafe_trail.size_x
		self.ant_starting_orientation = santafe_trail.ant_starting_orientation
		self.ant_starting_position = santafe_trail.ant_starting_position
		self.food_coordinates = santafe_trail.food_coordinates

	def export_trail(self):
		santafe_trail = SantaFeTrail((self.size_y, self.size_x), self.ant_starting_position,
		                             self.ant_starting_orientation, self.food_coordinates)
		santafe_trail.validate()
		return santafe_trail

	def build_trail(self, santafe_trail:SantaFeTrailBuilder):
		self.size_y, self.size_x = santafe_trail.dimensions
		cell_size = self._get_grid_edge_size()
		self._editing = True

		# draw trail for editing
		self.reset()
		self.draw()
		messagebox.showinfo('SantaFe Custom Trail Builder', 'LMouse - food\nRMouse - ant\nESC - save')

		# interactive callbacks
		def _set_food(event):
			x, y = event.x // cell_size, event.y // cell_size
			if (y, x) in self.food_coordinates:
				self.food_coordinates.remove((y, x))
			else:
				self.food_coordinates.add((y, x))
			self.reset()
			self.draw()

		def _set_ant(event):
			x, y = event.x // cell_size, event.y // cell_size
			self.ant_starting_position = (y, x)
			self.ant_starting_orientation = AntOrientation((self.ant_starting_orientation.value + 1) % 4)
			self.reset()
			self.draw()

		def _break(_):
			self._editing = False
		# -------------------------------------------

		self.canvas.bind('<Button-1>', _set_food)
		self.canvas.bind('<Button-3>', _set_ant)
		self.canvas.bind('<Escape>', _break)
		self.canvas.pack()
		self.canvas.focus_set()

		while (self._editing):
			self.canvas.update_idletasks()
			self.canvas.update()

		self.canvas.unbind('<Button-1>')
		self.canvas.unbind('<Button-3>')
		self.canvas.unbind('<Escape>')
	# ------------------------------------------------------------


	# Ant simulation
	# ------------------------------------------------------------
	def move_ahead(self):
		if self.step_count > self.max_steps:
			return
		self.step_count += 1

		y, x = self.ant.get_ahead_cell_coordinates()
		if x < 0 or y < 0 or x >= self.size_x or y >= self.size_y:
			return

		self.ant.move_ahead()
		self._ant_mark_visited()

		if self.animate_step_delay:
			self.draw()
			sleep(self.animate_step_delay)

	def turn_left(self):
		if self.step_count > self.max_steps:
			return
		self.ant.turn_left()
		self.step_count += 1

		if self.animate_step_delay:
			self.draw()
			sleep(self.animate_step_delay)

	def turn_right(self):
		if self.step_count > self.max_steps:
			return
		self.ant.turn_right()
		self.step_count += 1

		if self.animate_step_delay:
			self.draw()
			sleep(self.animate_step_delay)

	def is_food_ahead(self):
		y, x = self.ant.get_ahead_cell_coordinates()
		if x < 0 or y < 0 or x >= self.size_x or y >= self.size_y:
			return False
		if self.grid_visited[y, x]:
			return False
		return self.grid_has_food[y, x]

	# simulation helpers
	@staticmethod
	def if_then_else(condition, out1, out2):
		out1() if condition() else out2()

	def if_food_ahead(self, out1, out2):
		return partial(SantaFeSimulator.if_then_else, self.is_food_ahead, out1, out2)

	def _ant_mark_visited(self):
		if self.grid_has_food[self.ant.y, self.ant.x] and not self.grid_visited[self.ant.y, self.ant.x]:
			self.food_eaten += 1
		self.grid_visited[self.ant.y, self.ant.x] = True

	def execute_routine(self, routine, animate_step_delay=None):
		self.reset()
		self.animate_step_delay = animate_step_delay

		while self.step_count < self.max_steps:
			routine()

		self.animate_step_delay = None
	# ------------------------------------------------------------


	# Drawing
	# ------------------------------------------------------------
	def _get_grid_edge_size(self):
		grid_width, grid_height = self.canvas.winfo_width(), self.canvas.winfo_height()
		step_size_y = grid_height // self.size_y
		step_size_x = grid_width // self.size_x
		return min(step_size_x, step_size_y)

	def draw(self):
		if self.canvas is None:
			return

		self.canvas.delete(tk.ALL)

		grid_cell_size = self._get_grid_edge_size()
		height = (self.size_y) * grid_cell_size + 1
		width = (self.size_x) * grid_cell_size + 1

		# draw color cells
		for y in range(self.size_y):
			for x in range(self.size_x):
				color = None
				if self.grid_has_food[y, x] and self.grid_visited[y, x]:
					color = 'light green'
				elif self.grid_visited[y, x]:
					color = 'light blue'
				elif self.grid_has_food[y, x]:
					color = 'red'

				if color:
					self.canvas.create_rectangle(x*grid_cell_size, y*grid_cell_size, (x+1)*grid_cell_size, (y+1)*grid_cell_size, fill=color)

		# draw cell borders
		for x in range(1, width+grid_cell_size, grid_cell_size):
			self.canvas.create_line((x, 1), (x, height))
		for y in range(1, height+grid_cell_size, grid_cell_size):
			self.canvas.create_line((1, y), (width, y))

		# draw ant
		self.ant.ant_image_tk = PIL.ImageTk.PhotoImage(self.ant.ant_image) # keep reference!
		self.canvas.create_image(self.ant.x * grid_cell_size + grid_cell_size // 2, self.ant.y * grid_cell_size + grid_cell_size // 2, image=self.ant.ant_image_tk)

		# draw info
		self.canvas.create_text((self.size_x + 1)  * grid_cell_size + 50, 50, font="Times 18 bold",
		                        text='Food {}/{}\nSteps {}'.format(self.food_eaten, len(self.food_coordinates), self.step_count))

		self.canvas.update()
	# ------------------------------------------------------------


# sample run - santafe trail builder
if __name__ == '__main__':
	canvas_width, canvas_height = 1024, 786  # px
	grid_width, grid_height = 32, 32  # cells
	trail_out_file_name = 'test_trail.pkl'  # out file name

	# init TK
	master = tk.Tk(className='SantaFe')
	canvas = tk.Canvas(master, width=canvas_width, height=canvas_height)
	canvas.pack()
	canvas.update()

	# run simulation in trail builder mode
	SantaFeSimulator(SantaFeTrailBuilder((grid_height, grid_width)), None, canvas, trail_out_file_name)