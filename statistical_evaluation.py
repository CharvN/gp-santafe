#!/usr/bin/env python3
'''
    File name: evaluate_gp_params.py
    Author: Michal Charvát
    xcharv16@stud.fit.vutbr.cz
    FIT Brno University of Technology
    Date created: 25 Apr 2019
'''
from gp_santafe import SantaFeGP
from santafe_simulator import SantaFeTrail, AntOrientation

import matplotlib.pyplot as plt
import numpy as np

# multiple evolutions to evaluate parameters
if __name__ == "__main__":
	EVOLUTION_RUN_COUNT = 30

	# init plots
	plt.ion()
	figure_plot = plt.figure(figsize=(10, 6))
	fitness_plot = figure_plot.add_subplot(121)
	size_plot = figure_plot.add_subplot(122)

	# statistics
	size_values = list()
	size_average = list()
	fitness_values = list()
	fitness_average = list()

	try:
		for i in range(EVOLUTION_RUN_COUNT):
			print('Evolution run:  {}/{}\n______________________________'.format(i, EVOLUTION_RUN_COUNT))

			# available kwargs for SantaFeGP constructor
			# CANVAS_WIDTH
			# CANVAS_HEIGHT
			# SANTAFE_TRAIL_IN_FILE
			# ANT_MAX_STEPS
			# POPULATION_SIZE
			# GEN_SIZE
			# CROSS_PROB
			# MUT_PROB
			# HALL_OF_FAME_SIZE
			# TOURNAMENT_SIZE
			# DRAW_TRAIL
			santafe_gp = SantaFeGP(DRAW_TRAIL=False, POPULATION_SIZE=50, GEN_SIZE=20)
			best_individual, population, logbook = santafe_gp.evolve(should_plot=False)

			# get result values
			fitness_values.append(best_individual.fitness.values[0])
			fitness_average.append(np.mean(fitness_values))
			size_values.append(len(best_individual))
			size_average.append(np.mean(size_values))

			# plot
			fitness_plot.clear()
			fitness_plot.plot(fitness_values, label='Fitness')
			fitness_plot.plot(fitness_average, label='Average')
			fitness_plot.set_xlabel('Evolution number')
			fitness_plot.set_ylabel('Fitness')
			fitness_plot.legend()
			size_plot.clear()
			size_plot.plot(size_values, label='Size')
			size_plot.plot(size_average, label='Average')
			size_plot.set_xlabel('Evolution number')
			size_plot.set_ylabel('Size')
			size_plot.legend()
			plt.pause(0.0001)

			# destruct
			del santafe_gp

	except KeyboardInterrupt:
		pass

	print('\nFinal results:\n########################\n')
	print('Evolution executed: {}\nMax fitness: {}\nAverage fitness: {}\nMin fitness: {}\nStandard fitness deviation: {}\n'
	      'Max size: {}\nAverage size: {}\nMin size: {}\nStandard size deviation: {}\n'.format(
		EVOLUTION_RUN_COUNT, np.max(fitness_values), np.mean(fitness_values), np.min(fitness_values), np.std(fitness_values),
		np.max(size_values), np.mean(size_values), np.min(size_values), np.std(size_values)))
	print('########################\n')
	plt.ioff()
	plt.show()