# SantaFe trail problem - genetic programming
### EVO 2018/19 project FIT BUT

V rámci projektu byla vytvořena demonstrační / vizualizační aplikace využívající techniku genetického programování k řešení problému Santa Fe Trail, kde se snažíme vytvořit program ovládající uměleho mravence tak, aby dokázal v poli najít všechny kousky potravy. 


<img src="docs/santafe.png"
     style="float: left; margin-right: 15px;" />



Jedinec populace je reprezentován stromem, funkční množina, množinu terminálů a fitness funkce (počet snězené potravy) jsou definovat stejně jako v původní publikaci Johna Kozy\[1\].

Demonstrační aplikace implementován v jazyce Python s využitím knihovny DEAP\[2\], která poskytuje zázemí pro rychlé prototypování v oblasti evolučních algoritmů. V průběhu optimalizace je vizualizován nejlepší jedinec populace grafem jeho programu a v podobě cesty, kterou mravenec projde vykonáním jeho programu. Dále je průběžně vykreslován graf maximální, průměrné, minimální fitness populace a velikosti programu jedince. 


<img src="docs/gp_progress.png"
     style="float: left; margin-right: 15px;" />


Aplikace umožnuje interaktivní vytváření cest pro mravence včetně počáteční pozice a natočení mravence. Ten je možné spustit pomocí skriptu `./santafe_simulator.py`. Dále je možné pro evoluci nastavovat různé parametry jako maximální počet kroků, pravděpodobnosti křížení a mutace, velikost populace, velikost výběrového turnaje, počet generací a velikost síně slávy. Evoluce s vykreslováním se spouští skriptem `./gp_santafe.py`.

Po dokončení evoluce se vybere nejlepší jedinec populace a vykonání jeho programu se po krocích animuje na modelu cesty santafe. Průběh evoluce lze předčasně ukončit a přejít do módu animace nejlepšího jedince pomocí `SIGINT`.

Aplikace také umožnuje statistické vyhodnocení úspěšnosti evoluce při daných parametrech tím. Opakované spouštění evoluce se provede skriptem `./statistical_evaluation.py`. Průběžné výsledky evolucí jsou vynášeny do grafu, zachycujeme fitness a velikost programu jedince včetně průměru. Po dokončení všech běhů se na standardní výstup vypíší dosažené výsledky: minimální, maximální, průměrná hodnota fitness a velikosti nejlepších jedinců evolucí včetně standardních odchylek od průměru.

<img src="docs/statistics.png"
     style="float: left; margin-right: 15px;" />

### Výstup statistického běhu:
```
Final results:
########################

Evolution executed: 30
Max fitness: 47.0
Average fitness: 15.133333333333333
Min fitness: 8.0
Standard fitness deviation: 8.00305497225209
Max size: 68
Average size: 14.6
Min size: 3
Standard size deviation: 11.516944039110376

########################
```

### Použité knihovny
tkinter, numpy, matplotlib, DEAP, networkx, graphviz, pygraphviz, PIL, pickle

### Reference
1. Koza, John R., Genetic Programming: On the Programming of Computers by Means of Natural Selection. MIT Press, Cambridge, MA. 1992. pp. 147-155. Print

2. Distributed Evolutionary Algorithms in Python – DEAP library https://github.com/DEAP/deap